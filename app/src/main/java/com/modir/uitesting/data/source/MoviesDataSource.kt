package com.modir.uitesting.data.source

import com.modir.uitesting.data.Movie

interface MoviesDataSource {

    fun getMovie(movieId: Int): Movie?
}
package com.modir.uitesting.camera

import android.app.Activity.RESULT_OK
import android.app.Instrumentation
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Bundle
import android.provider.MediaStore
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.Intents.intending
import androidx.test.espresso.intent.matcher.IntentMatchers.hasData
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.modir.uitesting.R
import com.modir.uitesting.camera.ImageViewHasDrawableMatcher.hasDrawable
import com.modir.uitesting.testGallery.TestGalleryActivity
import org.hamcrest.Matcher
import org.hamcrest.Matchers.not
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4ClassRunner::class)
class CameraTest {

    @get: Rule
    val intentsTestRule = IntentsTestRule(CameraActivity::class.java)

    @Test
    fun test_cameraIntents_isBitmapSetImageView() {

        // GIVEN
        val expectedIntent: Matcher<Intent> = hasData(MediaStore.ACTION_IMAGE_CAPTURE)
        val activityResult = createImageActivityResultStub()
        intending(expectedIntent).respondWith(activityResult)

        // Execute and Verify
        onView(withId(R.id.image)).check(matches(not(hasDrawable())))
        onView(withId(R.id.button_launch_camera)).perform(click())
        intending(expectedIntent)
        onView(withId(R.id.image)).check(matches((hasDrawable())))
    }

    private fun createImageActivityResultStub(): Instrumentation.ActivityResult {
        val bundle = Bundle()
        bundle.putParcelable(
            KEY_IMAGE_DATA,
            BitmapFactory.decodeResource(
                intentsTestRule.activity.resources,
                R.drawable.ic_launcher_background
            )
        )

        val resultData = Intent()
        resultData.putExtras(bundle)
        return Instrumentation.ActivityResult(RESULT_OK, resultData)
    }
}
package com.modir.uitesting

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.platform.app.InstrumentationRegistry
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4ClassRunner::class)
class ExampleInstrumentedTest {
    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        assertEquals("com.modir.uitesting", appContext.packageName)
    }

//    @Test
//    fun test_navSecondaryActivity() {
//        val activityScenario = ActivityScenario.launch(MainActivity::class.java)
//
//        onView(withId(R.id.button)).perform(click())
//        onView(withId(R.id.second)).check(matches(isDisplayed()))
//    }

//    @Test
//    fun test_backPress_toMainActivity() {
//        val activityScenario = ActivityScenario.launch(MainActivity::class.java)
//
//        onView(withId(R.id.button)).perform(click())
//        onView(withId(R.id.second)).check(matches(isDisplayed()))

//        onView(withId(R.id.button1)).perform(click()) // method 1
//        pressBack() // method 2
//        onView(withId(R.id.main)).check(matches(isDisplayed()))
//    }

    //    @Test
//    fun test_isActivityInView() {
//        val activityScenario = ActivityScenario.launch(MainActivity::class.java)
//
//        onView(withId(R.id.main)).check(matches(isDisplayed()))
//    }
//
//    @Test
//    fun test_visibility_title_nextButton() {
//        val activityScenario = ActivityScenario.launch(MainActivity::class.java)
//        onView(withId(R.id.textView)).check(matches(isDisplayed())) // method 1
//        onView(withId(R.id.button)).check(matches(withEffectiveVisibility(Visibility.VISIBLE))) // method 2
//    }
//
//    @Test
//    fun test_isTitleDisplayed() {
//        val activityScenario = ActivityScenario.launch(MainActivity::class.java)
//
//        onView(withId(R.id.textView)).check(matches(withText("Main Activity")))
//    }
}
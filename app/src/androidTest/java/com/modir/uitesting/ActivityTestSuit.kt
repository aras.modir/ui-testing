package com.modir.uitesting

import org.junit.runner.RunWith
import org.junit.runners.Suite

@RunWith(Suite::class)
@Suite.SuiteClasses(
    ExampleInstrumentedTest::class,
    SecondActivityTest::class
)
class ActivityTestSuit